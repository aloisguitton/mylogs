import React, {Component} from "react";
import {Button, IconButton, InputAdornment, Snackbar, TextField, Typography} from "@mui/material";
import {Visibility, VisibilityOff} from "@mui/icons-material";
import {put, get} from "../Service/Requests";
import {Alert} from "@mui/lab";

export class settings extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showPassword: false,
            emailHost: "",
            emailUser: "",
            emailPassword: "",
            emailPort: "",
        }
    }

    componentDidMount() {
        get('/smtp')
            .then((res) => {
                let keys = Object.keys(res.data)
                keys.map((k) => {
                    if(k !== "success"){
                        this.setState({
                            [k]: res.data[k]
                        }, () => {
                            console.log(this.state)
                        })
                    }
                })
            })
    }

    onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        }, () => {
            console.log(this.state)
        })
    }

    render() {
        return <>
            <Typography variant={"h2"}>SMTP Server</Typography>
            <div className="row">
                <TextField onChange={this.onChange}
                           className={"col-12 col-md-6 px-1 my-2"}
                           name="emailHost"
                           value={this.state.emailHost}
                           size={'small'}
                           variant="filled"
                           type={"text"}
                           label={"Email host"}/>
                <TextField onChange={this.onChange}
                           className={"col-12 col-md-6 px-1 my-2"}
                           name="emailPort"
                           value={this.state.emailPort}
                           size={'small'}
                           variant="filled"
                           type={"number"}
                           label={"Email port"}/>
                <TextField onChange={this.onChange}
                           className={"col-12 col-md-6 px-1 my-2"}
                           name="emailUser"
                           value={this.state.emailUser}
                           size={'small'}
                           variant="filled"
                           type={"text"}
                           label={"Email user"}/>
                <TextField onChange={this.onChange}
                           className={"col-12 col-md-6 px-1 my-2"}
                           name="emailPassword"
                           value={this.state.emailPassword}
                           size={'small'}
                           variant="filled"
                           type={this.state.showPassword ? "text" : "password"}
                           label={"Email password"}
                           InputProps={{
                               endAdornment: <InputAdornment position="end">
                                   <IconButton
                                       aria-label="toggle password visibility"
                                       onClick={() => {
                                           this.setState({showPassword: this.state.showPassword !== true})
                                       }}
                                       edge="end"
                                   >
                                       {this.state.showPassword ? <Visibility/> : <VisibilityOff/>}
                                   </IconButton>
                               </InputAdornment>
                           }}/>
                <div className="col-12 text-md-end text-center">
                    <Button variant={"outlined"} size={"small"} onClick={() => {
                        put("/smtp", {
                            emailHost: this.state.emailHost,
                            emailUser: this.state.emailUser,
                            emailPassword: this.state.emailPassword,
                            emailPort: this.state.emailPort,
                        })
                            .then(() => {
                                this.setState({
                                    success: true
                                })
                            })
                            .catch((z) => {
                                console.log(z)
                                this.setState({
                                    error: true
                                })
                            })

                    }}>
                        Modify SMTP parameters
                    < /Button>
                </div>
            </div>


            <Snackbar open={this.state.error} autoHideDuration={6000} onClose={() => {
                this.setState({error: false})
            }}>
                <Alert onClose={() => {
                    this.setState({error: false})
                }} severity="error" variant={'filled'} sx={{width: '100%'}}>
                    Une erreur s'est produite.
                </Alert>
            </Snackbar>
            <Snackbar open={this.state.success} autoHideDuration={6000} onClose={() => {
                this.setState({success: false})
            }}>
                <Alert onClose={() => {
                    this.setState({success: false})
                }} severity="success" variant={'filled'} sx={{width: '100%'}}>
                    Vos modifications ont bien été enregistrées.
                </Alert>
            </Snackbar>
        </>
    }
}