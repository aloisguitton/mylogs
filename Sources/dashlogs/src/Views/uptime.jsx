import React, {Component} from "react";
import {get, put} from "../Service/Requests";
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DatePicker from '@mui/lab/DatePicker';
import TextField from '@mui/material/TextField';
import {
    Button,
    FormControl,
    IconButton,
    InputLabel,
    MenuItem,
    OutlinedInput,
    Select,
    Snackbar,
    Typography
} from "@mui/material";
import {Chart} from "react-google-charts";
import {Alert} from "@mui/lab";
import {Modal} from "react-bootstrap";
import {AddCircleOutlineOutlined, AddOutlined, HighlightOff} from "@mui/icons-material";
import {validateUrl} from "../Service/Service";

const io = require("socket.io-client");


export class uptime extends Component {
    constructor(props) {
        super(props);
        this.state = {
            socket: null,
            datas: [],
            startDate: null,
            endDate: null,
            sitesFilter: [],
            sites: [],
            sitesModal: false
        }
    }

    componentDidMount() {
        const args = process.argv.slice(2)
        const host = args[0] || process.env.REACT_APP_SERVER_HOST

        get("/uptime").then(r => {
            this.setState({
                datas: r.data.data
            })
        })

        get("/uptimeSites").then(r => {
            this.setState({
                sites: r.data.sites
            })
        })

        this.setState({
            socket: io("http://" + host + ":3500", {transports: ['websocket']})
        }, () => {
            this.state.socket.emit('join', "dashboard", (error) => {
                if (error) {
                    alert(error)
                }
            })
            this.state.socket.on('data', (datas) => {
                this.setState({
                    datas: [...this.state.datas, datas]
                })
            })
        })
    }

    render() {
        let datas = [...this.state.datas]
        if (this.state.sitesFilter.length > 0) {
            datas = datas.filter(d => this.state.sitesFilter.indexOf(d.site) > -1)
        }
        if (this.state.startDate) {
            datas = datas.filter(d => new Date(d.timestamp) > this.state.startDate.setHours(0, 0, 0, 0))
        }
        if (this.state.endDate) {
            datas = datas.filter(d => new Date(d.timestamp) < this.state.endDate.setHours(23, 59, 59, 999))
        }

        let pieData = [
            ['Status code', 'count']
        ], pieDataJson = {}
        let calendarData = [
            [{type: 'date', id: 'Date'}, {type: 'number', id: 'UP/DOWN'}],
        ], calendarDataJson = {}
        let lineData = [
            ['Date']
        ], lineDataJson = {}

        datas.map(d => {
            if (!lineData[0].includes(d.site)) {
                lineData[0].push(d.site)
            }

            if (pieDataJson.hasOwnProperty(d.statusCode)) {
                pieDataJson[d.statusCode] += 1
            } else {
                pieDataJson[d.statusCode] = 1
            }
            if (calendarDataJson.hasOwnProperty((new Date(d.timestamp)).setHours(0, 0, 0, 0))) {
                if (d.statusCode !== 200) {
                    calendarDataJson[(new Date(d.timestamp)).setHours(0, 0, 0, 0)] = -1
                }
            } else {
                if (d.statusCode === 200) {
                    calendarDataJson[(new Date(d.timestamp)).setHours(0, 0, 0, 0)] = 1
                } else {
                    calendarDataJson[(new Date(d.timestamp)).setHours(0, 0, 0, 0)] = -1
                }
            }
        })

        datas.map((d) => {
            if (lineDataJson.hasOwnProperty(d.timestamp)) {
                lineDataJson[d.timestamp][d.site] = d.requestTime
            } else {
                let v = {}
                lineData[0].slice(1).map(s => {
                    v[s] = null
                })
                lineDataJson[d.timestamp] = v
                lineDataJson[d.timestamp][d.site] = d.requestTime
            }
        })

        for (let i in pieDataJson)
            pieData.push([i, pieDataJson[i]]);

        for (let i in calendarDataJson)
            calendarData.push([new Date(parseInt(i)), calendarDataJson[i]]);

        for (let i in lineDataJson) {
            let t = [new Date(i)]
            for (let j in lineDataJson[i]) {
                t.push(lineDataJson[i][j])
            }
            lineData.push(t);
        }

        let sslTable = []

        if (lineData[0].slice(1).length > 0) {
            lineData[0].slice(1).map(site => {
                let index = [...datas].reverse().findIndex(s => s.site === site);
                index = index >= 0 ? datas.length - 1 - index : index
                sslTable.push(datas[index])
            })
        }

        return <>
            <h1>Dashboard</h1>
            <div className="row">
                <div className={"col-12 px-2 py-2"}>
                    <Button variant={"outlined"} color={"primary"} onClick={() => {this.setState({sitesModal: true})}}>My sites</Button>
                </div>
            </div>
            {
                datas.length
                    ? <>
                        <LocalizationProvider dateAdapter={AdapterDateFns}>
                            <div className="row">

                                <div className="col-4 px-2">
                                    <DatePicker
                                        label="Start date"
                                        value={this.state.startDate}
                                        onChange={(newValue) => {
                                            this.setState({
                                                startDate: new Date(newValue)
                                            })
                                        }}
                                        renderInput={(params) => <TextField {...params}
                                                                            className={"w-100"} size={"small"}/>}
                                    />
                                </div>
                                <div className="col-4 px-2">
                                    <DatePicker
                                        label="End date"
                                        value={this.state.endDate}
                                        onChange={(newValue) => {
                                            this.setState({
                                                endDate: new Date(newValue)
                                            })
                                        }}
                                        renderInput={(params) => <TextField {...params} className={"w-100"}
                                                                            size={"small"}/>}
                                    />
                                </div>
                                <div className="col-4 px-2">
                                    <FormControl className={"w-100"} size="small">
                                        <InputLabel>Sites</InputLabel>
                                        <Select
                                            value={this.state.sitesFilter}
                                            input={<OutlinedInput label="Sites"/>}
                                            multiple
                                            onChange={(e) => {
                                                this.setState({
                                                    sitesFilter: e.target.value
                                                })
                                            }}
                                            className={"w-100"}
                                        >
                                            {
                                                [...new Set(this.state.datas.map(a => a.site))].map((s) => {
                                                    return <MenuItem value={s}>{s}</MenuItem>
                                                })
                                            }
                                        </Select>
                                    </FormControl>

                                </div>

                            </div>
                        </LocalizationProvider>
                        <div className="row w-100">
                            <div className="col-md-4">
                                <Chart
                                    chartType="PieChart"
                                    data={pieData}
                                    options={{
                                        title: 'Response code',
                                    }}
                                />
                            </div>
                            <div className="col-md-8 overflow-scroll">
                                <Chart
                                    width={'1000px'}
                                    chartType="Calendar"
                                    data={calendarData}
                                    options={{
                                        title: 'UP/DOWN',
                                        colorAxis: {minValue: -1, maxValue: 1, colors: ['#c70000', '#3aad00']}
                                    }}
                                    rootProps={{'data-testid': '2'}}
                                />
                            </div>
                            <div className="col-md-12">
                                <Chart
                                    height={'30vh'}
                                    chartType="LineChart"
                                    loader={<div>Loading Chart</div>}
                                    data={lineData}
                                    options={{
                                        interpolateNulls: true,
                                        curveType: 'function',
                                        hAxis: {
                                            format: 'dd/MM/yyyy hh:mm'
                                        },
                                    }}
                                />
                            </div>
                            <div className="col-md-12">
                                {
                                    sslTable.map(s => {
                                        return <>
                                            <hr/>
                                            <div className="row">
                                                <div className="col-md-2 col-3 order-1 order-md-0">
                                                    {s.statusCode === 200 ? <span className={"green"}>Up</span> :
                                                        <span className={"red"}>Down</span>}
                                                </div>
                                                <div className="col-md-3 col-4 order-1 order-md-0">{s.label}</div>
                                                <div
                                                    className="col-md-2 col-5 order-1 order-md-0">{s.validSsl ? "Expire in " + s.daysRemainingSsl + " days" : "-"}</div>
                                                <div
                                                    className="col-md-5 col-12 order-0 order-md-1 mb-2 mb-md-0">{s.site}</div>
                                            </div>
                                        </>
                                    })
                                }
                            </div>
                        </div>


                    </>
                    : <>
                        <div className="w-100">
                            <div className="data-null-error mx-auto my-5 py-5">
                                <Alert variant="outlined" severity="info">
                                    Aucune donnée disponible
                                </Alert>
                            </div>
                        </div>
                    </>
            }

            <Modal show={this.state.sitesModal} onHide={() => {this.setState({sitesModal: false})}}>
                <Modal.Header closeButton>
                    <Modal.Title>Sites</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="row">
                        {
                            this.state.sites.map((s, i) => {
                                return <div
                                    className="col-12 d-flex justify-content-between align-items-center">
                                    <Typography>{s}</Typography>
                                    <IconButton aria-label="delete"
                                                color="primary"
                                                onClick={() => {
                                                    let sites = [...this.state.sites]
                                                    sites.splice(i, 1)
                                                    this.setState({
                                                        sites: sites
                                                    })
                                                }}
                                    >
                                        <HighlightOff/>
                                    </IconButton>
                                </div>
                            })
                        }
                        <div className="col-12">
                            <form onSubmit={(e) => {
                                e.preventDefault()
                                let sites = [...this.state.sites]
                                let value = document.getElementById("addSite").value
                                if(validateUrl(value) && sites.indexOf(value) === -1){
                                    sites.push(value)
                                    this.setState({
                                        sites: sites
                                    })
                                    document.getElementById("addSite").value = ""
                                }
                            }}>
                                <TextField variant={"outlined"} label={"New site"} size={"small"} id={"addSite"}/>
                                <IconButton aria-label="add"
                                            color="primary"
                                            type={"submit"}
                                >
                                    <AddCircleOutlineOutlined/>
                                </IconButton>
                            </form>
                        </div>
                        <div className="col-12 text-center mt-2">
                            <Button variant={"outlined"} color={"primary"} onClick={() => {
                                this.state.socket.emit('sites', this.state.sites)
                                this.setState({
                                    sitesModal: false
                                })
                            }}>Enregistrer</Button>
                        </div>
                    </div>
                </Modal.Body>
            </Modal>
            <Snackbar open={this.state.error} autoHideDuration={6000} onClose={() => {
                this.setState({error: false})
            }}>
                <Alert onClose={() => {
                    this.setState({error: false})
                }} severity="error" variant={'filled'} sx={{width: '100%'}}>
                    Une erreur s'est produite.
                </Alert>
            </Snackbar>
            <Snackbar open={this.state.success} autoHideDuration={6000} onClose={() => {
                this.setState({success: false})
            }}>
                <Alert onClose={() => {
                    this.setState({success: false})
                }} severity="success" variant={'filled'} sx={{width: '100%'}}>
                    Vos modifications ont bien été enregistrées.
                </Alert>
            </Snackbar>
        </>


    }
}