import React, {Component} from "react";
import {Button, Checkbox, Chip, Snackbar, Typography} from '@mui/material';
import TextField from '@mui/material/TextField';
import {Alert, Autocomplete} from "@mui/lab";
import {checkEmailAddr} from "../Service/Service";
import {get, put} from "../Service/Requests";

export class alerting extends Component {
    constructor(props) {
        super(props);
        this.state = {
            success: false,
            error: false,
            emails: [],
            alerting: [
                {
                    agent: "Uptime",
                    value: [
                        {name: "ssl", enabled: true, value: 30}
                    ]
                }
            ]
        }
    }

    componentDidMount() {
        get("/alerting")
            .then((res) => {
                let alerting = this.state.alerting
                res.data.alerting.map(a => {
                    alerting[alerting.findIndex(s => s.agent === a.agent)] = a
                })
                this.setState({
                    alerting: alerting,
                    emails: res.data.emails
                })
            })
    }

    changeAlert = (service, app, event) => {
        let alerts = this.state.alerting
        let v = alerts[service].value[app]

        if (event.target.type === "checkbox") {
            v[event.target.name] = event.target.checked
        } else {
            v[event.target.name] = event.target.value
        }
        alerts[service].value[app] = v

        this.setState({
            alerting: alerts
        })
    }

    render() {
        return <>
            {
                this.state.alerting.map((service, i) => {
                    return <div>
                        <Typography variant={"h1"}>{service.agent}</Typography>
                        {
                            service.value.map((v, j) => {
                                switch (v.name){
                                    case "ssl":
                                        return <div className={"row"}>
                                            <div className="col-auto d-flex align-items-center">
                                                <Checkbox name="enabled" checked={v.enabled} onChange={(e) => {
                                                    this.changeAlert(i, j, e)
                                                }}/>
                                            </div>
                                            <div className="col d-flex align-items-center">
                                                <Typography className={"d-flex align-items-center"}>
                                                    Notify me
                                                    <TextField
                                                        onChange={(e) => {
                                                            this.changeAlert(i, j, e)
                                                        }}
                                                        name="value"
                                                        value={v.value}
                                                        style={{width: "5vw"}}
                                                        className={"mx-1 input-text"}
                                                        size={'small'}
                                                        variant="filled"
                                                        type={"number"}
                                                    />
                                                    days before the expiration of the ssl certificate
                                                </Typography>
                                            </div>
                                        </div>
                                    case "requestTime":
                                        return <div className="row">
                                            <div className="col-auto d-flex align-items-center">
                                                <Checkbox name="enabled" checked={v.enabled} onChange={(e) => {
                                                    this.changeAlert(i, j, e)
                                                }}/>
                                            </div>
                                            <div className="col d-flex align-items-center">
                                                <Typography className={"d-flex align-items-center"}>
                                                    Notify me when the response time is greater than
                                                    <TextField
                                                        onChange={(e) => {
                                                            this.changeAlert(i, j, e)
                                                        }}
                                                        name="value"
                                                        value={v.value}
                                                        style={{width: "10vw"}}
                                                        className={"mx-1 input-text"}
                                                        size={'small'}
                                                        variant="filled"
                                                        type={"number"}
                                                    />
                                                    ms
                                                </Typography>
                                            </div>
                                        </div>
                                }

                            })
                        }
                    </div>
                })
            }
            <div className="br"/>
            <Autocomplete
                multiple
                options={[]}
                freeSolo
                value={this.state.emails}
                onChange={(event, newValue) => {
                    if (newValue.length === 0 || checkEmailAddr(newValue[newValue.length - 1])) {
                        this.setState({
                            emails: newValue,
                            emailError: false
                        })
                    } else {
                        this.setState({
                            emailError: true
                        })
                    }
                }}
                renderTags={(value, getTagProps) =>
                    value.map((option, index) => (
                        <Chip variant="outlined" label={option} {...getTagProps({index})} />
                    ))
                }
                renderInput={(params) => (
                    <TextField
                        {...params}
                        variant="filled"
                        label="Adresses email de contact"
                    />
                )}
            />
            <div className="br"/>
            <div className="text-center">
                <Button variant={"contained"} color={"primary"} onClick={() => {
                    put("/alerting", {
                        alerting: this.state.alerting,
                        email: this.state.emails
                    })
                        .then(() => {
                            this.setState({
                                success: true
                            })
                        })
                        .catch(() => {
                            this.setState({
                                error: true
                            })
                        })
                }}>Enregistrer</Button>
            </div>
            <Snackbar open={this.state.emailError} autoHideDuration={6000} onClose={() => {
                this.setState({emailError: false})
            }}>
                <Alert onClose={() => {
                    this.setState({emailError: false})
                }} severity="error" variant={'filled'} sx={{width: '100%'}}>
                    Vous devez ajouter une adresse email.
                </Alert>
            </Snackbar>
            <Snackbar open={this.state.error} autoHideDuration={6000} onClose={() => {
                this.setState({error: false})
            }}>
                <Alert onClose={() => {
                    this.setState({error: false})
                }} severity="error" variant={'filled'} sx={{width: '100%'}}>
                    Une erreur s'est produite.
                </Alert>
            </Snackbar>
            <Snackbar open={this.state.success} autoHideDuration={6000} onClose={() => {
                this.setState({success: false})
            }}>
                <Alert onClose={() => {
                    this.setState({success: false})
                }} severity="success" variant={'filled'} sx={{width: '100%'}}>
                    Vos modifications ont bien été enregistrées.
                </Alert>
            </Snackbar>
        </>
    }
}