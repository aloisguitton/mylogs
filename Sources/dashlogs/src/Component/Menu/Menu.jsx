import React, {Component, createRef} from "react";
import "./Menu.css"
import {Button, IconButton, Typography} from "@mui/material";
import {ChevronRightSharp, DashboardRounded, MailOutline, MoreTimeRounded, SettingsOutlined} from "@mui/icons-material";
import * as ReactDOM from "react-dom";

export class Menu extends Component {
    constructor(props) {
        super(props);
        this.state = {}
        this.menu = createRef()
        this.buttons = createRef()
    }

    componentDidMount() {
        let array = this.menu.current.getElementsByClassName("MuiButton-root")
        Array.from(array).map((i) => {
            let label = i.getElementsByClassName("menu-button-label")
            i.style.left = -(label[0].clientWidth + 9) + "px"
        })
    }

    getTitle = () => {
        switch (window.location.pathname){
            case "/uptime":
                return "Uptime"
            case "/alerting":
                return "Alerting"
            case "/settings":
                return "Settings"
            default:
                return null
        }
    }

    render() {

        return <>
            <div id="menu" ref={this.menu}>
                <div id="horizontal" className={"d-flex align-items-center px-2"}>
                    <Typography variant={"h2"} component={"p"}>
                        {this.getTitle()}
                    </Typography>
                </div>
                <div id="vertical">
                    {/*<Button endIcon={<DashboardRounded/>} href={"/"}>*/}
                    {/*    <span className={"menu-button-label"}>Dashboards</span>*/}
                    {/*</Button>*/}
                    <Button className="button" endIcon={<MoreTimeRounded/>} href={"/uptime"}>
                        <span className={"menu-button-label"}>Uptime</span>
                    </Button>
                    <Button className="button" endIcon={<MailOutline/>} href={"/alerting"}>
                        <span className={"menu-button-label"}>Alerting</span>
                    </Button>
                    <div className="bottom">
                        <Button className="button" endIcon={<SettingsOutlined/>} href={"/settings"}>
                            <span className={"menu-button-label"}>Settings</span>
                        </Button>
                        <div className="button d-none d-md-flex" onClick={() => {
                            this.menu.current.classList.toggle("expanded")
                            document.getElementById("mylogs-container").classList.toggle('menu-expanded')
                        }}>
                            <Button className="button" endIcon={<ChevronRightSharp/>}>
                                <span className={"menu-button-label"}>Collapse</span>
                            </Button>
                        </div>
                    </div>
                </div>
            </div>
        </>
    }
}