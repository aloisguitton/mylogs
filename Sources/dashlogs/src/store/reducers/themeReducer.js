const initialState = {
    theme: "theme-light"
}

function theme(state = initialState, action) {
    switch (action.type) {
        case 'SWITCH':
            let theme = state.theme === "theme-dark" ? 'theme-light' : "theme-dark"
            return {
                theme: theme
            };
        default:
            return state
    }
}

export default theme