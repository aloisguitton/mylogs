import React from 'react';
import {Route, Switch, Redirect} from "react-router-dom"
import {uptime} from "./Views/uptime";
import {alerting} from "./Views/alerting";
import {connect} from "react-redux";
import {settings} from "./Views/settings";

const mapStateToProps = (state) => {
    return state
}

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch: (action) => {
            dispatch(action)
        }
    }
}

function App(props) {
    const PrivateRoute = ({component: Component, ...rest}) => {
        return (
            <Route {...rest} render={(routeProps) => (
                props.authReducer.loggedIn
                    ? <Component {...routeProps} />
                    : <Redirect to='/'/>
            )}/>
        )
    }

    const IsNotConnectedRoute = ({component: Component, ...rest}) => (
        <Route {...rest} render={(routeProps) => (
            !props.authReducer.loggedIn
                ? <Component {...routeProps} />
                : <Redirect to='/dashboard'/>
        )}/>
    )

    return (
        <Switch>
            <Route exact path='/' component={uptime}/>
            <Route exact path='/uptime' component={uptime}/>
            <Route exact path='/alerting' component={alerting}/>
            <Route exact path='/settings' component={settings}/>
        </Switch>
    );
}

export default connect(mapStateToProps, mapDispatchToProps)(App)