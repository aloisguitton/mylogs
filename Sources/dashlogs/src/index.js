import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {BrowserRouter} from 'react-router-dom'
import {createTheme, ThemeProvider} from '@mui/material/styles';
import {frFR} from "@mui/material/locale";
import {Provider} from "react-redux";
import {store, persistor} from "./store/store"
import {PersistGate} from "redux-persist/integration/react";
import {Menu} from "./Component/Menu/Menu";

import "./style.css"
import "./assets/CSS/base.css"
import "./assets/CSS/Bootstrap/Modal.css"

require('dotenv').config()

const theme = createTheme({
    palette: {
        primary: {
            main: '#559cff',
        },
        secondary: {
            main: '#fa63aa'
        }
    },
    typography: {
        h1: {
            fontSize: "1.875rem",
            fontWeight: 700,
            fontFamily: "'Lato', sans-serif"
        },
        h2: {
            fontSize: "1.563rem",
            fontWeight: 700,
            fontFamily: "'Lato', sans-serif"
        },
        h3: {
            fontSize: "1.5rem",
            fontWeight: 400,
            fontFamily: "'Lato', sans-serif"
        },
        h4: {
            fontSize: "1.4rem",
            fontWeight: 400,
            fontFamily: "'Lato', sans-serif"
        },
        h5: {
            fontSize: "1.25rem",
            fontWeight: 400,
            fontFamily: "'Lato', sans-serif"
        },
        h6: {
            fontSize: "1.1rem",
            fontWeight: 400,
            fontFamily: "'Lato', sans-serif"
        },
        body1: {
            fontSize: "1rem",
            fontWeight: 300,
            fontFamily: "'Open Sans', sans-serif"
        }
    },
    zIndex: {
        appBar: 1000
    }
}, frFR)

ReactDOM.render(
    <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
            <BrowserRouter>
                <ThemeProvider theme={theme}>
                    <Menu/>
                    <div id="mylogs-container">
                        <App/>
                    </div>
                </ThemeProvider>
            </BrowserRouter>
        </PersistGate>
    </Provider>,
    document.getElementById('root')
);

