const cron = require('node-cron');
const https = require('https');
const http = require('http');
const sslChecker = require("ssl-checker")
const io = require("socket.io-client");
const axios = require('axios');

const args = process.argv.slice(2)

const socket = io("http://" + args[0] + ":3500", {transports: ['websocket']});

const instance = axios.create({
    httpsAgent: new https.Agent({
        rejectUnauthorized: false
    })
});

let sites = []
let tasks = []

console.log("Starting...")

socket.on("connect", () => {
    socket.emit('join', 'uptime')

    socket.on('sites', (s) => {
        sites = s
        main()
    })

    instance({
        method: 'get',
        url: "http://" + args[0] + ":3500/uptimeSites",
    })
        .then((res) => {
            sites = res.data.sites
            main()
        })

    const sendData = (label, site, data) => {
        let value = {
            label: label,
            site: site,
            ...data
        }
        console.log(value)
        socket.emit('data', value)
    }

    const main = () => {
        tasks.map(task => {
            task.stop()
        })
        tasks = []
        sites.map(s => {
            let site = new URL(s)
            let name = site.hostname.split('.')
            name = name[name.length - 2]
            name = name[0].toUpperCase() + name.slice(1)

            let task = cron.schedule("0 */5 * * * *", () => {
                const start = new Date();
                let timestamp = new Date((new Date(start)).setMilliseconds(0))
                if(site.protocol === "https:"){
                    https.get({hostname: site.hostname, path: site.pathname, port: site.port !== '' ? site.port : 443}, function(res) {
                        sslChecker(site.hostname, 'GET', 443).then(result => {
                            sendData(name, s, {requestTime: new Date() - start, validSsl: result.valid, daysRemainingSsl: result.daysRemaining, statusCode: res.statusCode, timestamp: timestamp})
                        });
                    });
                } else {
                    http.get({hostname: site.hostname, port: site.port !== '' ? site.port : 80}, function(res) {
                        sendData(name, s, {requestTime: new Date() - start, statusCode: res.statusCode, timestamp: timestamp})
                    });
                }
            });
            tasks.push(task)
        })
    }
});


