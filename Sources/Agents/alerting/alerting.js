const cron = require('node-cron');
const https = require('https');
const io = require("socket.io-client");
const axios = require('axios');
const nodemailer = require('nodemailer')

const args = process.argv.slice(2)
const socket = io("http://" + args[0] + ":3500", {transports: ['websocket']});

const instance = axios.create({
    httpsAgent: new https.Agent({
        rejectUnauthorized: false
    })
});

let todaySend = []
let alerting = {
    ssl: {enabled: false, value: 0, field: "daysRemainingSsl", compare: "<"},
    requestTime: {enabled: false, value: 0, field: "requestTime:", compare: ">"},
}
let SMTPconfig = {}
let emailList = []

console.log("Starting...")


socket.on("connect", () => {
    cron.schedule("0 0 * * *", () => {
        todaySend = []
    });

    if (Object.keys(SMTPconfig).length === 0) {
        instance({
            method: 'get',
            url: "http://" + args[0] + ":3500/smtp",
        })
            .then((res) => {
                let keys = Object.keys(res.data)
                keys.map(k => {
                    if (k !== "success") SMTPconfig[k] = res.data[k]
                })
            })
    }

    instance({
        method: 'get',
        url: "http://" + args[0] + ":3500/alerting",
    })
        .then((res) => {
            emailList = res.data.emails
            let arr = res.data.alerting.filter(a => a.agent = "Uptime")
            arr.map(a => {
                a.value.map((c) => {
                    alerting[c.name] = {
                        ...alerting[c.name],
                        enabled: c.enabled,
                        value: c.value,
                        field: c.field,
                    }
                })
            })
        })


    socket.emit('join', "alerting", (error) => {
        throw new Error()
    })

    socket.on('alerting', (v) => {
        if (Object.keys(SMTPconfig).length > 0) {
            Object.keys(alerting).map((alert) => {
                if (alerting[alert].enabled) {
                    if (alerting[alert].field in v &&
                        (alert !== "ssl" || alert === "ssl" && !todaySend.includes(v.site)) &&
                        (
                            (alerting[alert].compare === "<" && v[alerting[alert].field] <= alerting[alert].value) ||
                            (alerting[alert].compare === ">" && v[alerting[alert].field] >= alerting[alert].value)
                        )) {
                        todaySend.push(v.site)
                        emailList.map(email => {
                            switch (alert) {
                                case "ssl":
                                    Email(email, "SSL certificate expiration", `Your SSL certificate for ${v.site} will expire in ${v[alerting[alert].field]} days`)
                                    break;
                                case "requestTime":
                                    Email(email, "Website response time", `Your website ${v.site} took ${v[alerting[alert].field]} to respond`)
                                    break;
                            }
                        })


                    }
                }
            })
        }
        if (alerting.ssl.enabled && Object.keys(SMTPconfig).length > 0) {

        }
    })
});


const Email = (email, subject, value) => {
    let transporter = nodemailer.createTransport({
        host: SMTPconfig.emailHost,
        port: SMTPconfig.emailPort,
        auth: {
            user: SMTPconfig.emailUser,
            pass: SMTPconfig.emailPassword
        }
    });

    let mailOptions = {
        from: SMTPconfig.emailUser,
        to: email,
        subject: subject,
        html: value
    };
    transporter.sendMail(mailOptions, function (error, response) {
        if (error) {
            console.log(error);
        }
    });
}