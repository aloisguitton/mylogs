const mysql = require("mysql");
const util = require('util');
const dbConfig = require("../config/db.config.js");

console.log(dbConfig)
// Create a connection to the database
const connection = mysql.createConnection({
    host: dbConfig.HOST,
    user: dbConfig.USER,
    password: dbConfig.PASSWORD,
    database: dbConfig.DB,
    port: dbConfig.PORT,
    timezone: 'utc'
});

// open the MySQL connection
connection.connect(error => {
    if (error) throw error;
});

const query = util.promisify(connection.query).bind(connection);

module.exports = query;