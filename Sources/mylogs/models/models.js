const sql = require("./db.js");

exports.insertData = (d, type) => {
    sql("INSERT INTO mylogs (date, type, value) VALUES (?, ?, ?)", [new Date(), type, JSON.stringify(d)])
        .then((res) => {
        })
        .catch((e) => {
            console.log(e)
        })
}

exports.getUptime = async () => {
    let data = await sql("SELECT value FROM mylogs WHERE type = 'uptime' ORDER BY id")
    let returnData = []
    data.map(d => {
        returnData.push(JSON.parse(d.value))
    })
    return returnData;
}

exports.updateAlerting = async (alerting, email) => {
    await sql("DELETE from alerting")
    await sql("DELETE from config where name='email_alert'")
    for (let i = 0; i < alerting.length; i++) {
        await sql("INSERT INTO alerting (agent, value) VALUES (?, ?)", [alerting[i]['agent'], JSON.stringify(alerting[i]['value'])])
    }
    await sql("INSERT INTO config (name, value) VALUES ('email_alert', ?)", [JSON.stringify(email)])
}

exports.getAlerting = async () => {
    let alerting = await sql("SELECT agent, value FROM alerting")
    alerting.map((a) => {
        a.value = JSON.parse(a.value)
    })
    let emails = await sql("SELECT value FROM config where name = 'email_alert'")
    if (emails.length > 0) {
        emails = JSON.parse(emails[0]['value'])
    } else {
        emails = []
    }
    return {
        alerting: alerting,
        emails: emails
    }
}

exports.checkIfSSL = async () => {
    let ssl = (await sql("SELECT value FROM alerting WHERE agent='Uptime' AND value LIKE '%ssl%'"))
    return ssl.length > 0 && (JSON.parse(ssl[0]['value']))[0].enabled;
}

exports.getSMTP = async () => {
    return {
        emailHost: (await sql('SELECT value FROM config WHERE name="email_host"'))[0]['value'],
        emailPort: (await sql('SELECT value FROM config WHERE name="email_port"'))[0]['value'],
        emailUser: (await sql('SELECT value FROM config WHERE name="email_user"'))[0]['value'],
        emailPassword: (await sql('SELECT value FROM config WHERE name="email_password"'))[0]['value'],
    }
}

exports.updateSMTP = async (host, port, user, password) => {
    await sql("UPDATE config SET value = ? WHERE name = ?", [host, "email_host"])
    await sql("UPDATE config SET value = ? WHERE name = ?", [port, "email_port"])
    await sql("UPDATE config SET value = ? WHERE name = ?", [user, "email_user"])
    await sql("UPDATE config SET value = ? WHERE name = ?", [password, "email_password"])
    return true
}

exports.getSites = async () => {
    return JSON.parse((await sql("SELECT value FROM config WHERE name = ?", ["uptimes_sites"]))[0]['value'])
}

exports.updateSites = async (sites) => {
    await sql("UPDATE config SET value = ? WHERE name = ?", [JSON.stringify(sites), "uptimes_sites"])
    return true
}