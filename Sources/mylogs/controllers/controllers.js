const response = require('../Services/Response');
const Models = require('../models/models');

exports.getUptime = async (req, res, next) => {
    response.success(res, {data: await Models.getUptime()})
}

exports.updateAlerting = async (req, res) => {
    Models.updateAlerting(req.body.alerting, req.body.email)
        .then(() => {
            response.success(res)
        })
        .catch((e) => {
            response.error(res)
        })
}

exports.getAlerting = async (req, res) => {
    response.success(res, await Models.getAlerting())
}

exports.getSMTP = async (req, res) => {
    response.success(res, await Models.getSMTP())
}

exports.updateSMTP = async (req, res) => {
    Models.updateSMTP(req.body.emailHost, req.body.emailPort, req.body.emailUser, req.body.emailPassword)
        .then(() => {
            response.success(res)
        })
        .catch((e) => {
            response.error(res)
        })
}

exports.checkIfSSL = async (req, res) => {
    response.success(res, {ssl: await Models.checkIfSSL()})
}

exports.getSites = async (req, res) => {
    response.success(res, {sites: await Models.getSites()})
}