const socketio = require("socket.io")
const model = require("../models/models")

const clients = []

exports.ch_socket = (server) => {
    const io = socketio(server)


    io.on('connection', (socket) => {
        console.log(`Connecté au client ${socket.id}`)
        socket.on('data', (v) => {
            model.insertData(v, "uptime")
            io.in('dashboard').emit("data", {...v})
            io.in('alerting').emit("alerting", {...v})
        })

        socket.on('join', (r) => {
            addClient(socket.id, r)
            socket.join(r)
        })

        socket.on('sites', async (d) => {
            console.log(d)
            await model.updateSites(d)
            io.in("uptime").emit("sites", d)
        })

        socket.on('disconnect', () => {
            removeClient(socket.id)
        })
    })
}

const addClient = (id, room) => {
    room = room.trim().toLowerCase()

    const clientExist = clients.find((client) => client.id === id)

    if (clientExist) {
        return {error: "Client already exist"}
    }

    let client = {id, room}
    clients.push(client)

    return {client}
}

const changeRoom = (id, room) => {
    const index = clients.findIndex((client) => client.id === id)
    if (index !== -1) {
        clients[index]['room'] = room
    }
}

const removeClient = (id) => {
    const index = clients.findIndex((client) => client.id === id)

    if (index !== -1) {
        return clients.splice(index, 1)[0]
    }
}

const getClient = (id) => {
    return clients.find((client) => client.id === id)
}

const getSocket = (token) => {
    return clients.find((client) => client.client === token)
}