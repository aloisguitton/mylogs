const args = process.argv.slice(2)

console.log(args)

module.exports = {
    HOST: process.env.DBHOST || args[0]['HOST'],
    USER: process.env.DBUSER || args[0]['USER'],
    PASSWORD: process.env.DBPASSWORD || args[0]['PASSWORD'],
    DB: process.env.DBDB || args[0]['DB'],
    PORT: process.env.DBPORT || args[0]['PORT'],
};