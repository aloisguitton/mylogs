let express = require('express');
let router = express.Router();

const Ctrl = require('../controllers/controllers');

router.get('/uptime', Ctrl.getUptime);
router.put('/alerting', Ctrl.updateAlerting)
router.get('/alerting', Ctrl.getAlerting)
router.put('/smtp', Ctrl.updateSMTP)
router.get('/smtp', Ctrl.getSMTP)
router.get('/sslAlerting', Ctrl.checkIfSSL)
router.get('/uptimeSites', Ctrl.getSites)


module.exports = router;